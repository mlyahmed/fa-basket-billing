package com.financeactive.billing.bill.service;

import com.financeactive.billing.basket.entity.Basket;
import com.financeactive.billing.basket.factory.BasketFactory;
import com.financeactive.billing.basket.factory.BasketFactoryImpl;
import com.financeactive.billing.bill.entity.Bill;
import com.financeactive.billing.bill.factory.BillFactoryImpl;
import com.financeactive.billing.money.Amount;
import com.financeactive.billing.product.entity.Product;
import com.financeactive.billing.product.entity.ProductCategory;
import com.financeactive.billing.product.entity.ProductType;
import com.financeactive.billing.product.factory.ProductFactory;
import com.financeactive.billing.product.factory.ProductFactoryImpl;
import com.financeactive.billing.tax.factory.TaxCalculatorFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.stream.Stream;

import static com.financeactive.billing.money.Amount.ZERO_AMOUNT;
import static com.financeactive.billing.money.Amount.newAmount;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

class BillingServiceImplTest {

    private BasketFactory basketFactory;

    private BillingServiceImpl billingService;

    private ProductFactory productFactory;

    private Basket basket;

    private Bill bill;

    @BeforeEach
    void setup() {
        basketFactory = BasketFactoryImpl.instance();
        productFactory = ProductFactoryImpl.instance();
        billingService = new BillingServiceImpl();
        billingService.setBillFactory(BillFactoryImpl.instance());
        billingService.setTaxCalculator(TaxCalculatorFactory.instance());
        basket = basketFactory.createEmptyBasket();
    }

    @Test
    void when_empty_basket_then_return_blank_bill() {
        given_the_basket_is_empty();
        when_calculate_the_bill();
        then_the_bill_is_blank();
    }

    @ParameterizedTest
    @ValueSource(strings = {"12.23", "13.09", "123.24", "652.87"})
    void when_book_then_return_a_bill_with_zero_tax(final String amount) {
        given_a_book_is_added_to_the_basket_with(amount);
        when_calculate_the_bill();
        then_the_bill_total_is(newAmount(amount));
        then_the_bill_total_tax_is(ZERO_AMOUNT);
    }

    @ParameterizedTest
    @ValueSource(strings = {"16.98", "192.88", "1009.12", "5.65"})
    void when_medicine_then_return_a_bill_with_zero_tax(final String amount) {
        given_a_medicine_is_added_to_the_basket_with(amount);
        when_calculate_the_bill();
        then_the_bill_total_is(newAmount(amount));
        then_the_bill_total_tax_is(ZERO_AMOUNT);
    }

    @ParameterizedTest
    @ValueSource(strings = {"98.42", "87.19", "922.24", "986.07"})
    void when_two_chocolate_bars_then_apply_it_two_times(final String amount) {
        given_a_chocolate_bar_is_added_to_the_basket_with(amount);
        given_a_chocolate_bar_is_added_to_the_basket_with(amount);
        when_calculate_the_bill();
        then_the_bill_total_is(newAmount(amount).multiply(2));
        then_the_bill_total_tax_is(ZERO_AMOUNT);
    }

    private static Stream<Arguments> bookAndFoodExamples() {
        return Stream.of(
                Arguments.of("76.09", "5.54", "81.63"),
                Arguments.of("34.98", "19.63", "54.61"),
                Arguments.of("53.53", "3.87", "57.4")
        );
    }

    @ParameterizedTest
    @MethodSource("bookAndFoodExamples")
    void when_book_food_then_no_tax_applied(final String b, final String f, final String r) {
        given_a_book_is_added_to_the_basket_with(b);
        given_a_chocolate_bar_is_added_to_the_basket_with(f);
        when_calculate_the_bill();
        then_the_bill_total_is(newAmount(r));
        then_the_bill_total_tax_is(ZERO_AMOUNT);
    }

    private static Stream<Arguments> bookAndMusicAndChocolateExamples() {
        return Stream.of(
                Arguments.of("12.49", "14.99", "0.85", "29.83", "1.50"),
                Arguments.of("39.70", "25.29", "3.17", "70.71", "2.55"),
                Arguments.of("65.00", "5.71", "13.07", "84.38", "0.6")
        );
    }

    @ParameterizedTest
    @MethodSource("bookAndMusicAndChocolateExamples")
    //CHECKSTYLE:OFF
    void when_book_music_food_then_apply_regular_tax(String b, String m, String c, String tt, String tx) {
        given_a_book_is_added_to_the_basket_with(b);
        given_a_music_cd_is_added_to_the_basket_with(m);
        given_a_chocolate_bar_is_added_to_the_basket_with(c);
        when_calculate_the_bill();
        then_the_bill_total_is(newAmount(tt));
        then_the_bill_total_tax_is(newAmount(tx));
    }
    //CHECKSTYLE:ON

    private static Stream<Arguments> importedPricesExamples() {
        return Stream.of(
                Arguments.of("10", "10.5", "0.5"),
                Arguments.of("27.99", "29.39", "1.4"),
                Arguments.of("11.25", "11.85", "0.6")
        );
    }

    @ParameterizedTest
    @MethodSource("importedPricesExamples")
    void when_an_imported_chocolate_then_apply_overhead_rate(final String ht, final String ttc, final String tx) {
        given_an_imported_chocolate_bar_is_added_to_the_basket_with(ht);
        when_calculate_the_bill();
        then_the_bill_total_is(newAmount(ttc));
        then_the_bill_total_tax_is(newAmount(tx));
    }

    private void given_the_basket_is_empty() {
        basket = basketFactory.createEmptyBasket();
    }

    private void given_a_book_is_added_to_the_basket_with(final String price) {
        final ProductCategory category = productFactory.createCategory("BOOK");
        final ProductType type = productFactory.createType(category, "BOOK");
        final Product book = productFactory.createProduct("book-1", type, newAmount(price));
        basket.addItem(book);
    }

    private void given_a_medicine_is_added_to_the_basket_with(final String price) {
        final ProductCategory category = productFactory.createCategory("MEDICINE");
        final ProductType type = productFactory.createType(category, "MEDICINE");
        final Product book = productFactory.createProduct("medicine-1", type, newAmount(price));
        basket.addItem(book);
    }

    private void given_a_music_cd_is_added_to_the_basket_with(final String price) {
        final ProductCategory category = productFactory.createCategory("TUNES");
        final ProductType type = productFactory.createType(category, "MUSIC_CD");
        final Product musicCD = productFactory.createProduct("cd-1", type, newAmount(price));
        basket.addItem(musicCD);
    }

    private void given_a_chocolate_bar_is_added_to_the_basket_with(final String price) {
        final ProductCategory category = productFactory.createCategory("FOOD");
        final ProductType type = productFactory.createType(category, "CHOCOLATE_BAR");
        final Product chocolateBar = productFactory.createProduct("choc-1", type, newAmount(price));
        basket.addItem(chocolateBar);
    }

    private void given_an_imported_chocolate_bar_is_added_to_the_basket_with(final String price) {
        final ProductCategory category = productFactory.createCategory("FOOD");
        final ProductType type = productFactory.createType(category, "CHOCOLATE_BAR", true);
        final Product chocolateBar = productFactory.createProduct("choc-2", type, newAmount(price));
        basket.addItem(chocolateBar);
    }

    private void when_calculate_the_bill() {
        bill = billingService.calculateBill(basket);
    }

    private void then_the_bill_is_blank() {
        assertThat(bill, notNullValue());
        assertThat(bill.getTotal(), equalTo(ZERO_AMOUNT));
        assertThat(bill.getTotalTax(), equalTo(ZERO_AMOUNT));
    }

    private void then_the_bill_total_tax_is(final Amount amount) {
        assertThat(bill, notNullValue());
        assertThat(bill.getTotalTax(), equalTo(amount));
    }

    private void then_the_bill_total_is(final Amount amount) {
        assertThat(bill, notNullValue());
        assertThat(bill.getTotal(), equalTo(amount));
    }

}
