package com.financeactive.billing.bill.service;

import com.financeactive.billing.basket.entity.Basket;
import com.financeactive.billing.basket.factory.BasketFactory;
import com.financeactive.billing.basket.factory.BasketFactoryImpl;
import com.financeactive.billing.bill.entity.Bill;
import com.financeactive.billing.bill.factory.BillFactoryImpl;
import com.financeactive.billing.dictionary.factory.DictionaryAccessorFactory;
import com.financeactive.billing.product.entity.Product;
import com.financeactive.billing.product.entity.ProductCategory;
import com.financeactive.billing.product.entity.ProductType;
import com.financeactive.billing.product.exception.UndefinedProductTypeException;
import com.financeactive.billing.product.factory.ProductFactory;
import com.financeactive.billing.product.factory.ProductFactoryImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;

import static com.financeactive.billing.money.Amount.ZERO_AMOUNT;
import static com.financeactive.billing.money.Amount.newAmount;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertThrows;

class BillPublishServiceTextTest {


    private BasketFactory basketFactory;

    private ProductFactory productFactory;

    private Basket basket;

    private Bill bill;

    private String print;

    private BillPublishServiceText publisher;

    @BeforeEach
    void setup() {
        basketFactory = BasketFactoryImpl.instance();
        productFactory = ProductFactoryImpl.instance();
        publisher = new BillPublishServiceText();
        publisher.setDictionaryAccessor(DictionaryAccessorFactory.instance());
    }


    @Test
    void when_publish_a_blank_bill_then_print_zero() {
        basket = BasketFactoryImpl.instance().createEmptyBasket();
        bill = BillFactoryImpl.instance().createBill(basket, ZERO_AMOUNT, ZERO_AMOUNT);
        when_publish_the_bill();
        assertThat(print, equalTo("Montant des taxes : 0.00\nTotal : 0.00"));
    }

    @Test
    void when_publish_a_bill_with_one_item_then_publish_it() {
        basket = basketFactory.createEmptyBasket();
        given_a_book_is_added_to_the_basket_with("12.98");
        bill = BillFactoryImpl.instance().createBill(basket, newAmount("12.98"), ZERO_AMOUNT);
        when_publish_the_bill();

        StringBuilder builder = new StringBuilder();
        builder.append(String.format("1 livre : %s", "12.98"));
        builder.append("\n");
        builder.append(String.format("Montant des taxes : %s", bill.getTotalTax().asString()));
        builder.append("\n");
        builder.append(String.format("Total : %s", bill.getTotal().asString()));

        assertThat(print, equalTo(builder.toString()));
    }


    @Test
    void when_publish_a_bill_with_one_item_and_twice_the_product_then_publish_it() {
        basket = basketFactory.createEmptyBasket();
        given_a_book_is_added_to_the_basket_with("12.98");
        given_a_book_is_added_to_the_basket_with("12.98");

        bill = BillFactoryImpl.instance().createBill(basket, newAmount("25.96"), newAmount("2.96"));
        when_publish_the_bill();

        StringBuilder builder = new StringBuilder();
        builder.append(String.format("2 livre : %s", "25.96"));
        builder.append("\n");
        builder.append(String.format("Montant des taxes : %s", bill.getTotalTax().asString()));
        builder.append("\n");
        builder.append(String.format("Total : %s", bill.getTotal().asString()));
        assertThat(print, equalTo(builder.toString()));
    }


    @Test
    void when_publish_a_bill_with_two_items_then_publish_it() {
        basket = basketFactory.createEmptyBasket();
        given_a_book_is_added_to_the_basket_with("12.98");
        given_a_medicine_is_added_to_the_basket_with("23.98");

        bill = BillFactoryImpl.instance().createBill(basket, newAmount("36.96"), newAmount("9.18"));
        when_publish_the_bill();

        StringBuilder builder = new StringBuilder();
        builder.append(String.format("1 livre : %s", "12.98"));
        builder.append("\n");
        builder.append(String.format("1 boîte de pilules contre la migraine : %s", "23.98"));
        builder.append("\n");
        builder.append(String.format("Montant des taxes : %s", bill.getTotalTax().asString()));
        builder.append("\n");
        builder.append(String.format("Total : %s", bill.getTotal().asString()));
        assertThat(print, equalTo(builder.toString()));
    }

    @Test
    void when_the_product_type_is_undefined_then_error() {
        basket = basketFactory.createEmptyBasket();
        given_an_undefined_product_is_added_to_the_basket();
        bill = BillFactoryImpl.instance().createBill(basket, ZERO_AMOUNT, ZERO_AMOUNT);
        assertThrows(UndefinedProductTypeException.class, this::when_publish_the_bill);
    }


    private void given_a_book_is_added_to_the_basket_with(final String price) {
        final ProductCategory category = productFactory.createCategory("BOOK");
        final ProductType type = productFactory.createType(category, "FR-BOOK");
        final Product book = productFactory.createProduct("book-1", type, ZERO_AMOUNT);
        book.setTtc(newAmount(price));
        basket.addItem(book);
    }

    private void given_a_medicine_is_added_to_the_basket_with(final String price) {
        final ProductCategory category = productFactory.createCategory("MEDICINE");
        final ProductType type = productFactory.createType(category, "HEADACHE-PILLS-BOX");
        final Product book = productFactory.createProduct("medicine-1", type, ZERO_AMOUNT);
        book.setTtc(newAmount(price));
        basket.addItem(book);
    }

    private void given_an_undefined_product_is_added_to_the_basket() {
        final ProductCategory category = productFactory.createCategory("MEDICINE");
        final ProductType type = productFactory.createType(category, "HEADACHE-TV");
        final Product book = productFactory.createProduct("medicine-1", type, ZERO_AMOUNT);
        book.setTtc(newAmount("12.09"));
        basket.addItem(book);
    }

    private void when_publish_the_bill() {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        publisher.publish(bill, stream);
        print = new String(stream.toByteArray());
    }

}
