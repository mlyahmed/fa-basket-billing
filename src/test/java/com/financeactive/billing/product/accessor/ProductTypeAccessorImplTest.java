package com.financeactive.billing.product.accessor;

import com.financeactive.billing.product.entity.ProductType;
import com.financeactive.billing.product.factory.ProductCategoryAccessorFactory;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

class ProductTypeAccessorImplTest {

    private ProductTypeAccessorImpl accessor;

    @BeforeEach
    void setup() {
        accessor = new ProductTypeAccessorImpl();
        accessor.setCategoryAccessor(ProductCategoryAccessorFactory.instance());
    }

    @Test
    void when_find_chocolate_bar_by_code_then_return_it() {
        final ProductType type = accessor.findByCode("CHOCOLATE-BAR");
        assertThat(type, notNullValue());
        assertThat(type.getCode(), equalTo("CHOCOLATE-BAR"));
        assertThat(type.isImported(), equalTo(false));
        assertThat(type.getCategory(), notNullValue());
        assertThat(type.getCategory().getCode(), equalTo("FOOD"));
    }

    @Test
    void when_find_imported_chocolate_box_by_code_then_return_it() {
        final ProductType type = accessor.findByCode("CHOCOLATE-BOX-IMP");
        assertThat(type, notNullValue());
        assertThat(type.getCode(), equalTo("CHOCOLATE-BOX-IMP"));
        assertThat(type.isImported(), equalTo(true));
        assertThat(type.getCategory(), notNullValue());
        assertThat(type.getCategory().getCode(), equalTo("FOOD"));
    }

    @Test
    void when_find_headache_pills_by_code_then_return_it() {
        final ProductType type = accessor.findByCode("HEADACHE-PILLS-BOX");
        assertThat(type, notNullValue());
        assertThat(type.getCode(), equalTo("HEADACHE-PILLS-BOX"));
        assertThat(type.isImported(), equalTo(false));
        assertThat(type.getCategory(), notNullValue());
        assertThat(type.getCategory().getCode(), equalTo("MEDICINE"));
    }

    @ParameterizedTest
    @ValueSource(strings = {"KJKJS", "GHAT", "NBHG", "OKA"})
    void when_find_by_code_and_it_does_not_exist_then_return_null(final String value) {
        final ProductType type = accessor.findByCode(value);
        assertThat(type, Matchers.nullValue());
    }


}
