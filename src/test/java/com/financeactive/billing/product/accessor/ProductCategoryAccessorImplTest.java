package com.financeactive.billing.product.accessor;

import com.financeactive.billing.product.entity.ProductCategory;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;

class ProductCategoryAccessorImplTest {

    private ProductCategoryAccessorImpl accessor;

    @BeforeEach
    void setup() {
        accessor = new ProductCategoryAccessorImpl();
    }

    @ParameterizedTest
    @ValueSource(strings = {"BOOK", "MEDICINE", "TUNES"})
    void when_find_by_code_and_exists_then_return_it(final String value) {
        final ProductCategory category = accessor.findByCode(value);
        assertThat(category, notNullValue());
        assertThat(category.getCode(), Matchers.equalTo(value));
    }

    @ParameterizedTest
    @ValueSource(strings = {"BO", "MDCN", "TNS", "PRF"})
    void when_find_by_code_and_it_does_not_exist_then_return_null(final String value) {
        final ProductCategory category = accessor.findByCode(value);
        assertThat(category, Matchers.nullValue());
    }

}
