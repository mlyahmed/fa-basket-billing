package com.financeactive.billing.product.service;

import com.financeactive.billing.product.entity.ProductType;
import com.financeactive.billing.product.factory.ProductTypeAccessorFactory;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;

class ProductServiceImplTest {

    @Test
    void when_find_a_type_and_exists_then_return_it() {
        ProductServiceImpl productService = new ProductServiceImpl();
        productService.setProductTypeAccessor(ProductTypeAccessorFactory.instance());
        final ProductType productType = productService.getProductType("PARFUM");
        assertThat(productType, notNullValue());
    }

}
