package com.financeactive.billing.tax.rules;

import com.financeactive.billing.product.entity.Product;

public class OverheadTaxRule extends TaxRule {

    private static final double OVERHEAD_TAX = 0.05;

    @Override
    void process(final Product product) {
        if (product.isImported()) {
            product.setTax(product.getTax().plus(product.getHt().multiply(OVERHEAD_TAX)));
        }
    }

}
