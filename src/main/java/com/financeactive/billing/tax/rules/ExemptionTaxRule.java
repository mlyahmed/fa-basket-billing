package com.financeactive.billing.tax.rules;

import com.financeactive.billing.money.Amount;
import com.financeactive.billing.product.entity.Product;

import java.util.stream.Stream;

public class ExemptionTaxRule extends TaxRule {

    @Override
    void process(final Product product) {
        if (isExempted(product)) product.setTax(Amount.ZERO_AMOUNT);
    }

    private boolean isExempted(final Product product) {
        return Stream.of("BOOK", "FOOD", "MEDICINE").anyMatch(product::isCategory);
    }

}
