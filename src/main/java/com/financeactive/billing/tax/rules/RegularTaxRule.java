package com.financeactive.billing.tax.rules;

import com.financeactive.billing.product.entity.Product;

public class RegularTaxRule extends TaxRule {

    private static final double REGULAR_TAX = 0.1;

    @Override
    void process(final Product product) {
        product.setTax(product.getHt().multiply(REGULAR_TAX));
    }

}
