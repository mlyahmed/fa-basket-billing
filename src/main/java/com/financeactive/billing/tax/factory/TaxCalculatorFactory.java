package com.financeactive.billing.tax.factory;

import com.financeactive.billing.tax.calculator.TaxCalculator;
import com.financeactive.billing.tax.calculator.TaxCalculatorImpl;

public final class TaxCalculatorFactory {

    private static volatile TaxCalculator instance;

    private TaxCalculatorFactory() {
    }

    public static TaxCalculator instance() {
        if (instance == null) {
            synchronized (TaxCalculator.class) {
                if (instance == null)
                    instance = createTaxCalculator();
            }
        }
        return instance;
    }

    private static TaxCalculator createTaxCalculator() {
        final TaxCalculatorImpl taxCalculator = new TaxCalculatorImpl();
        taxCalculator.setRuleFactory(TaxRuleFactoryImpl.instance());
        taxCalculator.postCreation();
        return taxCalculator;
    }


}
