package com.financeactive.billing.tax.factory;

import com.financeactive.billing.tax.rules.ExemptionTaxRule;
import com.financeactive.billing.tax.rules.LastRoundTaxRule;
import com.financeactive.billing.tax.rules.OverheadTaxRule;
import com.financeactive.billing.tax.rules.RegularTaxRule;

public class TaxRuleFactoryImpl implements TaxRuleFactory {

    private static volatile TaxRuleFactoryImpl instance;

    public static TaxRuleFactoryImpl instance() {
        if (instance == null) {
            synchronized (TaxRuleFactoryImpl.class) {
                if (instance == null)
                    instance = new TaxRuleFactoryImpl();
            }
        }
        return instance;
    }

    @Override
    public RegularTaxRule createRegularTaxRule() {
        return new RegularTaxRule();
    }

    @Override
    public ExemptionTaxRule createExemptionTaxRule() {
        return new ExemptionTaxRule();
    }

    @Override
    public OverheadTaxRule createOverheadTaxRule() {
        return new OverheadTaxRule();
    }

    @Override
    public LastRoundTaxRule createLastRoundTaxRule() {
        return new LastRoundTaxRule();
    }

}
