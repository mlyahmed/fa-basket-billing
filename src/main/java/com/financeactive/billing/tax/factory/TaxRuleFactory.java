package com.financeactive.billing.tax.factory;

import com.financeactive.billing.tax.rules.ExemptionTaxRule;
import com.financeactive.billing.tax.rules.LastRoundTaxRule;
import com.financeactive.billing.tax.rules.OverheadTaxRule;
import com.financeactive.billing.tax.rules.RegularTaxRule;

public interface TaxRuleFactory {

    RegularTaxRule createRegularTaxRule();
    ExemptionTaxRule createExemptionTaxRule();
    OverheadTaxRule createOverheadTaxRule();
    LastRoundTaxRule createLastRoundTaxRule();

}
