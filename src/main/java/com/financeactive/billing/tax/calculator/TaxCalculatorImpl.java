package com.financeactive.billing.tax.calculator;

import com.financeactive.billing.product.entity.Product;
import com.financeactive.billing.tax.factory.TaxRuleFactory;
import com.financeactive.billing.tax.rules.ExemptionTaxRule;
import com.financeactive.billing.tax.rules.LastRoundTaxRule;
import com.financeactive.billing.tax.rules.OverheadTaxRule;
import com.financeactive.billing.tax.rules.RegularTaxRule;
import com.financeactive.billing.tax.rules.TaxRule;
import lombok.Setter;

public class TaxCalculatorImpl implements TaxCalculator {

    private @Setter TaxRuleFactory ruleFactory;

    private TaxRule chain;

    public void postCreation() {
        chain = chain();
    }

    @Override
    public void applyTax(final Product product) {
        chain.apply(product);
        product.setTtc(product.getHt().plus(product.getTax()));
    }

    private TaxRule chain() {
        final RegularTaxRule regular = ruleFactory.createRegularTaxRule();
        final ExemptionTaxRule exemption = ruleFactory.createExemptionTaxRule();
        final OverheadTaxRule overhead = ruleFactory.createOverheadTaxRule();
        final LastRoundTaxRule lastRound = ruleFactory.createLastRoundTaxRule();
        regular.setNextRule(exemption);
        exemption.setNextRule(overhead);
        overhead.setNextRule(lastRound);
        return regular;
    }

}
