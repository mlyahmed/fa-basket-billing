package com.financeactive.billing.tax.calculator;

import com.financeactive.billing.product.entity.Product;

public interface TaxCalculator {

    void applyTax(Product product);

}
