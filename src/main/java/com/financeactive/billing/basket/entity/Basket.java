package com.financeactive.billing.basket.entity;

import com.financeactive.billing.product.entity.Product;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class Basket {

    private final Map<String, BasketItem> items;

    public Basket() {
        this.items = new LinkedHashMap<>();
    }

    public Collection<BasketItem> getItems() {
        return items.values();
    }

    public void addItem(final Product product) {
        items.put(product.getId(), incrementQuantity(product));
    }

    private BasketItem incrementQuantity(final Product product) {
        return getItem(product) == null ? new BasketItem(product, 1) : getItem(product).increment();
    }

    private BasketItem getItem(final Product product) {
        return items.get(product.getId());
    }

}
