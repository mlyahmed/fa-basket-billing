package com.financeactive.billing.basket.factory;

import com.financeactive.billing.basket.entity.Basket;

public class BasketFactoryImpl implements BasketFactory {

    private static volatile BasketFactoryImpl instance;

    public static BasketFactoryImpl instance() {
        if (instance == null) {
            synchronized (BasketFactoryImpl.class) {
                if (instance == null)
                    instance = new BasketFactoryImpl();
            }
        }
        return instance;
    }

    @Override
    public Basket createEmptyBasket() {
        return new Basket();
    }

}
