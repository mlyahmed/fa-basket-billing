package com.financeactive.billing.bill.service;

import com.financeactive.billing.bill.entity.Bill;

import java.io.OutputStream;

public interface BillPublishService {

    void publish(Bill bill, OutputStream out);

}
