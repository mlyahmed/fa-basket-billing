package com.financeactive.billing.bill.service;

import com.financeactive.billing.basket.entity.BasketItem;
import com.financeactive.billing.bill.entity.Bill;
import com.financeactive.billing.dictionary.accessor.DictionaryAccessor;
import com.financeactive.billing.dictionary.entity.Dictionary;
import com.financeactive.billing.product.exception.UndefinedProductTypeException;
import lombok.Setter;

import java.io.OutputStream;

public class BillPublishServiceText implements BillPublishService {

    private static final String DOMAIN = "PRODUCT-TYPE";

    private @Setter DictionaryAccessor dictionaryAccessor;

    @Override
    public void publish(final Bill bill, final OutputStream out) {
        bill.getBasket().getItems().forEach(i -> writeBasketItem(out, i));
        write(out, buildTotalTaxStatement(bill));
        writeLineFeed(out);
        write(out, buildTotalStatement(bill));
    }

    private void writeBasketItem(final OutputStream out, final BasketItem item) {
        write(out, buildBasketItemStatement(item));
        writeLineFeed(out);
    }

    private String buildBasketItemStatement(final BasketItem item) {
        return String.format("%d %s : %s", item.getQuantity(), lookupBasketItemLabel(item), price(item));
    }

    private String price(final BasketItem item) {
        return item.getTotalTTC().asString();
    }

    private String lookupBasketItemLabel(final BasketItem item) {
        final Dictionary dic = dictionaryAccessor.findByDomainAndCode(DOMAIN, item.getProduct().getCode());
        if (dic == null) throw new UndefinedProductTypeException(item.getProduct().getCode());
        return dic.getLabel();
    }

    private String buildTotalTaxStatement(final Bill bill) {
        return String.format("Montant des taxes : %s", bill.getTotalTax().asString());
    }

    private String buildTotalStatement(final Bill bill) {
        return String.format("Total : %s", bill.getTotal().asString());
    }

    private void writeLineFeed(final OutputStream out) {
        write(out, "\n");
    }

    private void write(final OutputStream out, final String value) {
        try {
            out.write(value.getBytes());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
