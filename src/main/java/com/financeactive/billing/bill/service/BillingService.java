package com.financeactive.billing.bill.service;

import com.financeactive.billing.basket.entity.Basket;
import com.financeactive.billing.bill.entity.Bill;

public interface BillingService {

    Bill calculateBill(Basket basket);

}
