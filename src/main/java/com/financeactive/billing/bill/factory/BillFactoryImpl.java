package com.financeactive.billing.bill.factory;

import com.financeactive.billing.basket.entity.Basket;
import com.financeactive.billing.bill.entity.Bill;
import com.financeactive.billing.money.Amount;

public class BillFactoryImpl implements BillFactory {

    private static volatile BillFactoryImpl instance;

    public static BillFactoryImpl instance() {
        if (instance == null) {
            synchronized (BillFactoryImpl.class) {
                if (instance == null)
                    instance = new BillFactoryImpl();
            }
        }
        return instance;
    }

    public Bill createBill(final Basket basket, final Amount total, final Amount totalTax) {
        return new Bill(basket, total, totalTax);
    }

}
