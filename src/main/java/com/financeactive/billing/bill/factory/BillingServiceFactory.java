package com.financeactive.billing.bill.factory;

import com.financeactive.billing.bill.service.BillingService;
import com.financeactive.billing.bill.service.BillingServiceImpl;
import com.financeactive.billing.tax.factory.TaxCalculatorFactory;

public final class BillingServiceFactory {

    private static volatile BillingService instance;

    private BillingServiceFactory() {
    }

    public static BillingService instance() {
        if (instance == null) {
            synchronized (BillingService.class) {
                if (instance == null)
                    instance = createBillingService();
            }
        }
        return instance;
    }

    private static BillingService createBillingService() {
        final BillingServiceImpl billingService = new BillingServiceImpl();
        billingService.setBillFactory(BillFactoryImpl.instance());
        billingService.setTaxCalculator(TaxCalculatorFactory.instance());
        return billingService;
    }

}
