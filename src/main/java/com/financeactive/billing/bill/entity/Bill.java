package com.financeactive.billing.bill.entity;

import com.financeactive.billing.basket.entity.Basket;
import com.financeactive.billing.money.Amount;
import lombok.Data;

@Data
public class Bill {

    private final Basket basket;

    private final Amount total;

    private final Amount totalTax;



    public Bill(final Basket basket, final Amount total, final Amount totalTax) {
        this.basket = basket;
        this.total = total;
        this.totalTax = totalTax;
    }

}
