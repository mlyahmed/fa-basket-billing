package com.financeactive.billing.dictionary.accessor;

import com.financeactive.billing.dictionary.entity.Dictionary;

public interface DictionaryAccessor {

    Dictionary findByDomainAndCode(String domain, String code);

}
