package com.financeactive.billing.dictionary.factory;

import com.financeactive.billing.dictionary.entity.Dictionary;

public interface DictionaryFactory {

    Dictionary createDictionary(String domain, String code, String label);

}
