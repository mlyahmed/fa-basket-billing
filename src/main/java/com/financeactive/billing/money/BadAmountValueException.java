package com.financeactive.billing.money;

class BadAmountValueException extends RuntimeException {

    BadAmountValueException(final String value) {
        super(String.format("The Value <%s> is not valid", value));
    }
}
