package com.financeactive.billing;

import com.financeactive.billing.basket.entity.Basket;
import com.financeactive.billing.basket.factory.BasketFactoryImpl;
import com.financeactive.billing.bill.entity.Bill;
import com.financeactive.billing.bill.factory.BillPublishServiceFactory;
import com.financeactive.billing.bill.factory.BillingServiceFactory;
import com.financeactive.billing.bill.service.BillPublishService;
import com.financeactive.billing.bill.service.BillingService;
import com.financeactive.billing.product.entity.Product;
import com.financeactive.billing.product.entity.ProductType;
import com.financeactive.billing.product.factory.ProductFactory;
import com.financeactive.billing.product.factory.ProductFactoryImpl;
import com.financeactive.billing.product.factory.ProductServiceFactory;
import com.financeactive.billing.product.service.ProductService;

import static com.financeactive.billing.money.Amount.newAmount;

public final class Main {

    private ProductFactory productFactory;
    private ProductService productService;
    private BillingService billingService;
    private BillPublishService publishService;
    private Basket basket;

    private Main() {
        productFactory = ProductFactoryImpl.instance();
        productService = ProductServiceFactory.instance();
        billingService = BillingServiceFactory.instance();
        publishService = BillPublishServiceFactory.instance();
        basket = BasketFactoryImpl.instance().createEmptyBasket();
    }

    private void process() {
        //setBasket1();
        //setBasket2();
        setBasket3();
        final Bill bill = billingService.calculateBill(basket);
        publishService.publish(bill, System.out);
    }

    private void setBasket1() {
        addItem("fr-book-1", "FR-BOOK", "12.49");
        addItem("cd-musical-1", "MUSICAL-CD", "14.99");
        addItem("choc-bar-1", "CHOCOLATE-BAR", "0.85");
    }

    private void setBasket2() {
        addItem("choc-book-imp-1", "CHOCOLATE-BOX-IMP", "10.00");
        addItem("fragnancy-imp-1", "FRANGANCY-IMP", "47.50");
    }

    private void setBasket3() {
        addItem("fragnancy-imp-1", "FRANGANCY-IMP", "27.99");
        addItem("parfum-1", "PARFUM", "18.99");
        addItem("headache-pills-1", "HEADACHE-PILLS-BOX", "9.75");
        addItem("choc-book-imp-1", "CHOCOLATE-BOX-IMP", "11.25");
    }

    private void addItem(final String id, final String type, final String price) {
        final ProductType productType = productService.getProductType(type);
        final Product product = productFactory.createProduct(id, productType, newAmount(price));
        basket.addItem(product);
    }

    public static void main(final String[] args) {
        Main main = new Main();
        main.process();
    }

}
