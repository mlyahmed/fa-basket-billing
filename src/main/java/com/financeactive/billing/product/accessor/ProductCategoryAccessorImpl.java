package com.financeactive.billing.product.accessor;

import com.financeactive.billing.product.entity.ProductCategory;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import net.minidev.json.JSONArray;
import org.apache.commons.io.IOUtils;

import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.Set;

import static java.util.stream.Collectors.toSet;

public class ProductCategoryAccessorImpl implements ProductCategoryAccessor {

    @Override
    public ProductCategory findByCode(final String code) {
        return filter(loadCategories(), code);
    }

    private ProductCategory filter(final Set<ProductCategory> categories, final String code) {
        return categories.stream()
                .filter(d -> d.getCode().equals(code))
                .findFirst().orElse(null);
    }

    private Set<ProductCategory> loadCategories() {
        Object document = Configuration.defaultConfiguration().jsonProvider().parse(readDataFile());
        JSONArray values = JsonPath.read(document, "$.[*]");
        return values.stream()
                .map(m -> (Map) m)
                .map(m -> new ProductCategory(m.get("code").toString()))
                .collect(toSet());
    }


    private String readDataFile() {
        try {
            final InputStream stream = this.getClass().getResourceAsStream("/data/product-category.json");
            return IOUtils.toString(stream, Charset.forName("UTF-8"));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
