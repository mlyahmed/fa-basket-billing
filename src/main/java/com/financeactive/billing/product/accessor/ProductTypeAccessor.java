package com.financeactive.billing.product.accessor;

import com.financeactive.billing.product.entity.ProductType;

public interface ProductTypeAccessor {

    ProductType findByCode(String code);

}
