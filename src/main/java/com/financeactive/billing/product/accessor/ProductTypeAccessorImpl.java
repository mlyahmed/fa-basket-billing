package com.financeactive.billing.product.accessor;

import com.financeactive.billing.product.entity.ProductCategory;
import com.financeactive.billing.product.entity.ProductType;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import lombok.Setter;
import net.minidev.json.JSONArray;
import org.apache.commons.io.IOUtils;

import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.Set;

import static java.util.stream.Collectors.toSet;

public class ProductTypeAccessorImpl implements ProductTypeAccessor {

    private @Setter ProductCategoryAccessor categoryAccessor;

    @Override
    public ProductType findByCode(final String code) {
        return filter(loadTypes(), code);
    }

    private ProductType filter(final Set<ProductType> types, final String code) {
        return types.stream()
                .filter(d -> d.getCode().equals(code))
                .findFirst().orElse(null);
    }

    private Set<ProductType> loadTypes() {
        Object document = Configuration.defaultConfiguration().jsonProvider().parse(readDataFile());
        JSONArray values = JsonPath.read(document, "$.[*]");
        return values.stream()
                .map(m -> (Map) m)
                .map(m -> new ProductType(lookupCategory(m), lookupCode(m), lookupImported(m)))
                .collect(toSet());
    }

    private ProductCategory lookupCategory(final Map m) {
        return categoryAccessor.findByCode(m.get("category").toString());
    }

    private String lookupCode(final Map m) {
        return m.get("code").toString();
    }

    private boolean lookupImported(final Map m) {
        return Boolean.valueOf(m.get("imported").toString());
    }

    private String readDataFile() {
        try {
            final InputStream stream = this.getClass().getResourceAsStream("/data/product-type.json");
            return IOUtils.toString(stream, Charset.forName("UTF-8"));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


}
