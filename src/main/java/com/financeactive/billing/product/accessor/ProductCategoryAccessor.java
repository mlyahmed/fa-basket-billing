package com.financeactive.billing.product.accessor;

import com.financeactive.billing.product.entity.ProductCategory;

public interface ProductCategoryAccessor {

    ProductCategory findByCode(String code);

}
