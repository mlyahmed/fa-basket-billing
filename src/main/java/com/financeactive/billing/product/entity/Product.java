package com.financeactive.billing.product.entity;

import com.financeactive.billing.money.Amount;
import lombok.Data;

@Data
public class Product {

    private final String id;

    private final ProductType type;

    private final Amount ht;

    private Amount tax;

    private Amount ttc;

    public Product(final String id, final ProductType type, final Amount ht) {
        this.id = id;
        this.type = type;
        this.ht = ht;
    }


    public boolean isCategory(final String category) {
        return category.equals(type.getCategory().getCode());
    }

    public boolean isImported() {
        return type.isImported();
    }

    public String getCode() {
        return type.getCode();
    }
}
