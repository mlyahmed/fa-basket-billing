package com.financeactive.billing.product.service;

import com.financeactive.billing.product.accessor.ProductTypeAccessor;
import com.financeactive.billing.product.entity.ProductType;
import lombok.Setter;

public class ProductServiceImpl implements ProductService {

    private @Setter ProductTypeAccessor productTypeAccessor;

    @Override
    public ProductType getProductType(final String code) {
        return productTypeAccessor.findByCode(code);
    }

}
