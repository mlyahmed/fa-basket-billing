package com.financeactive.billing.product.service;

import com.financeactive.billing.product.entity.ProductType;

public interface ProductService {

    ProductType getProductType(String code);

}
